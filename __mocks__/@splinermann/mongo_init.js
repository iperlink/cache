const _ = require('lodash')

exports.connect = () => (
  { db: () => ({
    collection: () => ({
      deleteMany: (query, cb) => {
        if (!_.isArray(query.id.$in)) {
          cb({ message: 'delete error ' })
        }
        cb(null, [{
          thisWasInserted: 1,
        }])
      },
      insertMany: (results, cb) => {
        if (!results[0].id) {
          cb({ message: 'results must be an array' })
        }
        cb(null, [{
          thisWasInserted: 1,
        }])
      },
    }),
  }),
  other: ' aaa',
  })
