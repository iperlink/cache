const Joi = require('joi')
const _ = require('lodash')

exports.prepareSelectQuery = (...args) => {
  const A = Object.assign({}, args)
  try {
    const schema = Joi.object().keys({
      0: Joi.string(),
      4: Joi.array(),
    }).without('0', '2')
    Joi.validate(_.pick(A, ['0', '4']), schema, err => {
      if (err || A[1] !== 'testing') {
        throw (err)
      }
    })
    if (A[4][0] === '1') {
      return [{
        companyId: 1,
        email: 'mock@gmail.com',
        id: 1,
        name: 'andres',
        tags: 'andres||andre@gmail.com',
      }]
    }
    return []
  } catch (err) {
    throw ({ error: 'error in prepare select query' })
  }
}
