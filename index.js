const { cacheInit } = require('./dist/init')

const {
  cache,
} = require('./dist/cache')

exports.cache = cache
exports.cacheInit = cacheInit
