'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ = require('lodash');
var bluebird = require('bluebird');

var _require = require('@splinermann/parallel_funct'),
    runParallel = _require.runParallel;

var _require2 = require('@splinermann/error_logger'),
    customLogBack = _require2.customLogBack,
    undefinedIndex = _require2.undefinedIndex;

var _require3 = require('@splinermann/mysql_init'),
    makeMysqlPoolCluster = _require3.makeMysqlPoolCluster;

var _require4 = require('@splinermann/mysql_utils'),
    prepareSelectQuery = _require4.prepareSelectQuery;

var mongoOpPromise = function mongoOpPromise(db, name, ids, result, cb) {
  var collection = db.collection(name);
  collection.deleteMany({ id: { $in: ids } }, function (deleteErr) {
    if (deleteErr) {
      cb(customLogBack({
        err: 'Mongo err ' + deleteErr.message
      }, true, true));
    }
    collection.insertMany(result, function (err, inserted) {
      if (err) {
        console.log('mpngo err', err);
        cb(customLogBack({
          err: 'Mongo err ' + err.message
        }, true, true));
      }
      cb(null, inserted);
    });
  });
};

var mongoOp = bluebird.promisify(mongoOpPromise);

var getCache = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(params) {
    var client, cluster, db, name, dbList, id, query, queryString, queryStringWithId, results, tags;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            client = params.client, cluster = params.cluster, db = params.db, name = params.name, dbList = params.dbList, id = params.id, query = params.query;
            _context.prev = 1;
            queryString = undefinedIndex(query, 'queryText', 'query does not have query text');
            queryStringWithId = queryString + ' where ' + query.table + '.id = ?';
            _context.next = 6;
            return prepareSelectQuery(queryStringWithId, client, dbList, cluster, [id]);

          case 6:
            results = _context.sent;

            console.log(results);

            if (!results.length) {
              _context.next = 13;
              break;
            }

            _context.next = 11;
            return mongoOp(db, name, [id], results);

          case 11:
            tags = _.map(results, function (result) {
              return { aggregate: name, id: result.id, tags: result.tags };
            });
            return _context.abrupt('return', tags);

          case 13:
            return _context.abrupt('return', []);

          case 16:
            _context.prev = 16;
            _context.t0 = _context['catch'](1);

            console.log(_context.t0);
            return _context.abrupt('return', _context.t0);

          case 20:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined, [[1, 16]]);
  }));

  return function getCache(_x) {
    return _ref.apply(this, arguments);
  };
}();

var cache = function () {
  var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(msg, querys, dbList, cluster, mongoConn) {
    var client, id, queryName, table, clientdb, db, params, tags, tagParams;
    return _regenerator2.default.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            client = msg.client, id = msg.id, queryName = msg.queryName, table = msg.table;
            clientdb = undefinedIndex(dbList, client, 'client ' + client + ' does not exist');

            if (!clientdb.err) {
              _context2.next = 5;
              break;
            }

            return _context2.abrupt('return', clientdb);

          case 5:
            db = mongoConn.db(client);
            params = _.map(queryName, function (queryN) {
              var query = undefinedIndex(querys, queryN, 'query ' + queryN + ' does not exist');
              var param = {
                client: client,
                cluster: cluster,
                db: db,
                name: queryN,
                dbList: dbList,
                id: id,
                query: query,
                table: table
              };
              return param;
            });
            _context2.next = 9;
            return runParallel(params, getCache);

          case 9:
            tags = _context2.sent;
            tagParams = {
              client: client,
              tags: _.flatten(tags)
            };
            return _context2.abrupt('return', tagParams);

          case 14:
            _context2.prev = 14;
            _context2.t0 = _context2['catch'](0);
            return _context2.abrupt('return', _context2.t0);

          case 17:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, undefined, [[0, 14]]);
  }));

  return function cache(_x2, _x3, _x4, _x5, _x6) {
    return _ref2.apply(this, arguments);
  };
}();

exports.cache = cache;
exports.getCache = getCache;
//# sourceMappingURL=cache.js.map