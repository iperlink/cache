'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _require = require('@splinermann/lambdainit'),
    getCached = _require.getCached;

var _require2 = require('@splinermann/mongo_init'),
    connect = _require2.connect;

var _require3 = require('@splinermann/mysql_init'),
    makeMysqlPoolCluster = _require3.makeMysqlPoolCluster;

var _require4 = require('./cache'),
    setCache = _require4.setCache;

var cacheInit = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(Conn, Cluster, select) {
    var names, result, conn, cluster, querys;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            names = ['DB_LIST'];

            if (!Cluster) {
              names.push('HOST_LIST');
            }
            if (!select) {
              names.push('select');
            }
            _context.next = 6;
            return getCached(process.env.REDIS_HOST, names);

          case 6:
            result = _context.sent;
            _context.t0 = Conn;

            if (_context.t0) {
              _context.next = 12;
              break;
            }

            _context.next = 11;
            return connect({
              user: process.env.MONGO_USER,
              password: process.env.MONGO_PASSWORD,
              host: process.env.MONGO_HOST
            });

          case 11:
            _context.t0 = _context.sent;

          case 12:
            conn = _context.t0;

            console.log('mongo_connection', conn);

            cluster = Cluster || makeMysqlPoolCluster(result.HOST_LIST);
            querys = select || result.select;
            return _context.abrupt('return', (0, _extends3.default)({}, result, { conn: conn, querys: querys, cluster: cluster }));

          case 19:
            _context.prev = 19;
            _context.t1 = _context['catch'](0);


            console.log(_context.t1);

          case 22:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined, [[0, 19]]);
  }));

  return function cacheInit(_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}();

exports.cacheInit = cacheInit;
//# sourceMappingURL=init.js.map