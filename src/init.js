const { getCached } = require('@splinermann/lambdainit')
const { connect } = require('@splinermann/mongo_init')
const { makeMysqlPoolCluster } = require('@splinermann/mysql_init')
const { setCache } = require('./cache')

const cacheInit = async (Conn, Cluster, select) => {
  try {
    const names = ['DB_LIST']
    if(!Cluster) {
      names.push('HOST_LIST')
    }
    if(!select) {
      names.push('select')
    }
    const result = await getCached(process.env.REDIS_HOST,names)
    const conn = Conn || await connect({
      user: process.env.MONGO_USER,
      password: process.env.MONGO_PASSWORD,
      host: process.env.MONGO_HOST
    } )
    console.log('mongo_connection', conn)

    const cluster = Cluster || makeMysqlPoolCluster(result.HOST_LIST)
    const querys = select || result.select
    return { ...result, conn, querys, cluster }
  } catch (err) {

    console.log(err)
  }
}

exports.cacheInit = cacheInit
