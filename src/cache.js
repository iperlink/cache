const _ = require('lodash')
const bluebird = require('bluebird')
const { runParallel } = require('@splinermann/parallel_funct')

const {
  customLogBack,
  undefinedIndex,
} = require('@splinermann/error_logger')

const { makeMysqlPoolCluster } = require('@splinermann/mysql_init')
const { prepareSelectQuery } = require('@splinermann/mysql_utils')

const mongoOpPromise = (db, name, ids, result, cb) => {
  const collection = db.collection(name)
  collection.deleteMany({ id: { $in: ids } }, deleteErr => {
    if (deleteErr) {
      cb(customLogBack({
        err: `Mongo err ${deleteErr.message}`,
      }, true, true))
    }
    collection.insertMany(result, (err, inserted) => {
      if (err) {
        console.log('mpngo err', err)
        cb(customLogBack({
          err: `Mongo err ${err.message}`,
        }, true, true))
      }
      cb(null, inserted)
    })
  })
}

const mongoOp = bluebird.promisify(mongoOpPromise)

const getCache = async params => {
  const {
    client,
    cluster,
    db,
    name,
    dbList,
    id,
    query,
  } = params
  try {
    const queryString = undefinedIndex(query, 'queryText', 'query does not have query text')
    const queryStringWithId = `${queryString} where ${query.table}.id = ?`
    const results = await prepareSelectQuery(
      queryStringWithId,
      client,
      dbList,
      cluster,
      [id],
    )
    console.log(results)
    if (results.length) {
      await mongoOp(db, name, [id], results)
      const tags = _.map(results,
        result =>
          ({ aggregate: name, id: result.id, tags: result.tags }))
      return tags
    } return []
  } catch (err) {
    console.log(err)
    return err
  }
}

const cache = async (msg, querys, dbList, cluster, mongoConn) => {
  try {
    const {
      client,
      id,
      queryName,
      table,
    } = msg
    const clientdb = undefinedIndex(dbList, client, `client ${client} does not exist`)
    if (clientdb.err) {
      return clientdb
    }
    const db = mongoConn.db(client)
    const params = _.map(queryName, queryN => {
      const query = undefinedIndex(querys, queryN, `query ${queryN} does not exist`)
      const param = {
        client,
        cluster,
        db,
        name: queryN,
        dbList,
        id,
        query,
        table,
      }
      return param
    })
    const tags = await runParallel(params, getCache)
    const tagParams = {
      client,
      tags: _.flatten(tags),
    }
    return tagParams
  } catch (err) {
    return err
  }
}

exports.cache = cache
exports.getCache = getCache

