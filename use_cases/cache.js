const { _ } = require('lodash')

const { cacheInit } = require('../dist/init')
const { cache } = require('../dist/cache')

const msg = { 
  affectedRows: 1,
  client: 'testing',
  id: 1,
  queryName: ['user'],
  table: 'users',
}

cacheInit()
.then(result => {
  const {
    DB_LIST,
    conn,
    querys,
    cluster
  } = result
 console.log(conn)
  cache(msg, querys, DB_LIST, cluster, conn)
    .then(result =>
      { console.log(result) })
  // cluster = result.Cluster
  // querys = result.querys
  // insertFunction(msg, result.querys, result.DB_LIST, result.Cluster).
  //   then(function(res){
  //     console.log(res)
  //    })
 })