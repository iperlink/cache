const { connect } = require('@splinermann/mongo_init')

const { makeMysqlPoolCluster } = require('@splinermann/mysql_init')
const {
  getCache,
  cache,
} = require('../src/cache')
const MONGO_CONN = {
  user: 'developer',
}

const mongoConn = connect(MONGO_CONN)
const db = mongoConn.db('testing')
const params = {
  client: 'testing',
  cluster: true,
  db,
  name: 'users',
  host: {},
  id: '1',
  query: {
    queryText: 'select * from <%=db_name%>user ',
  },
}

const QUERYS = {
  "user": {
    "indexes": [
      {
        "id": 1
      },
      {
        "updated_at": -1
      }
    ],
    "limit": 1000,
    "mods": {
      "ussername": "in"
    },
    "queryText": " SELECT &lt;%=db_name%&gt;users.id, &lt;%=db_name%&gt;users.name, &lt;%=db_name%&gt;users.email,\n      company.id as companyId \n      , CONCAT(&lt;%=db_name%&gt;users.name,'||',  &lt;%=db_name%&gt;users.email) as tags\n      FROM &lt;%=db_name%&gt;users \n      INNER JOIN &lt;%=db_name%&gt;company_user on &lt;%=db_name%&gt;company_user.user_id=&lt;%=db_name%&gt;users.id \n      INNER JOIN &lt;%=db_name%&gt;company on &lt;%=db_name%&gt;company.id=&lt;%=db_name%&gt;company_user.company_id \n       ",
    "table": "users",
    "type": "select",
    "name": "user"
  },
  "user2": {
    "indexes": [
      {
        "id": 1
      },
      {
        "updated_at": -1
      }
    ],
    "limit": 1000,
    "mods": {
      "ussername": "in"
    },
    "queryText": " SELECT &lt;%=db_name%&gt;users.id, &lt;%=db_name%&gt;users.name, &lt;%=db_name%&gt;users.email,\n      company.id as companyId \n      , CONCAT(&lt;%=db_name%&gt;users.name,'||',  &lt;%=db_name%&gt;users.email) as tags\n      FROM &lt;%=db_name%&gt;users \n      INNER JOIN &lt;%=db_name%&gt;company_user on &lt;%=db_name%&gt;company_user.user_id=&lt;%=db_name%&gt;users.id \n      INNER JOIN &lt;%=db_name%&gt;company on &lt;%=db_name%&gt;company.id=&lt;%=db_name%&gt;company_user.company_id \n       ",
    "table": "users",
    "type": "select",
    "name": "user2"
  }
}
const msg = { 
  affectedRows: 1,
  client: 'testingss',
  id: 1,
  queryName: ['user'],
  table: 'users',
}
const HOST = [
  {
    "host": "localhost",
    "name": "default_select",
    "password": "$2a$04$vF5M4DexY7YUHEfT60dNLufJhyxA.NHzyAg1KSAjspivgZsn2LPGi",
    "user": "default_select",
    "port": "3306"
  }
]
const dbList = {
  "testing": {
    "dbName": "testing",
    "clientName": "client1",
    "hostName": "default"
  }
}
test('getCache', async () => {
  const ret = await getCache(params)
  console.log(ret)
  expect(ret[0].aggregate).toEqual('users')
})

test('getCache empty', async () => {
  const paramsError = {
    client: 'testing',
    cluster: true,
    db,
    functName: 'users',
    host: {},
    id: '122',
    query: {
      queryText: 'select * from <%=db_name%>user ',
    },
  }
  const ret = await getCache(paramsError)
  expect(ret).toEqual([])
})

test('getCache error no client', async () => {
  const paramsNoclient = {
    client: 'notesting',
    cluster: true,
    db,
    functName: 'users',
    host: {},
    id: '122',
    query: {
      queryText: 'select * from <%=db_name%>user ',
    },
  }
  const ret = await getCache(paramsNoclient)

  expect(ret.error).toEqual('error in prepare select query')
})

test('getCache error no client', async () => {
  const msgErr = { 
    affectedRows: 1,
    client: 'testingss',
    id: 1,
    queryName: ['user'],
    table: 'users',
  }
  const mongoConn = connect(MONGO_CONN)
  const cluster = makeMysqlPoolCluster()
  const res = await cache(msg, QUERYS, dbList, cluster, mongoConn)

  expect(res.err).toEqual('client testingss does not exist')
})

test('getCache error no query', async () => {
  const msg = {
    affectedRows: 1,
    client: 'testing',
    id: 1,
    queryName: ['baduser'],
    table: 'users',
  }
  const mongoConn = connect(MONGO_CONN)
  const cluster = makeMysqlPoolCluster()
  try {
    await cache(msg, QUERYS, HOST, cluster, mongoConn)
  } catch (err) {
    expect(err.error).toEqual('query baduser does not exist')
  }
})

test('getCache ', async () => {
  const msg = {
    affectedRows: 1,
    client: 'testing',
    id: '1',
    queryName: ['user', 'user2'],
    table: 'users',
  }
  const mongoConn = connect(MONGO_CONN)


  const cluster = makeMysqlPoolCluster()
  const ret = await cache(msg, QUERYS, dbList, cluster, mongoConn)
  const tags = [{"aggregate": "user", "id": 1, "tags": "andres||andre@gmail.com"}, {"aggregate": "user2", "id": 1, "tags": "andres||andre@gmail.com"}]
  expect(ret.tags).toEqual(tags)
})
